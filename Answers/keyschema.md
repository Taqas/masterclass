# KeySchema

```json
{
  "Resources" : {
    "DynamoDBTable" : {
      "Type" : "AWS::DynamoDB::Table",
      "Properties" : {
        "KeySchema" : [
          {
            "AttributeName" : "id",
            "KeyType": "HASH"
          }
        ]
      }
    }
  }
}
```
