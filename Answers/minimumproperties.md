# Minimum properties

The minimum properties that have to be configured to create a DynamoDB table are

```
AttributeDefinitions
  Name = id
  Type = S
KeySchema
  Name = id
  Type = HASH
ProvisionedThroughput
  ReadCapacityUnits = 5
  WriteCapacityUnits = 5
TableName = <yourTableName>
```
