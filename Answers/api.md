# API

```json
{
  "AWSTemplateFormatVersion" : "2010-09-09",
  "Parameters" : {
    "DynamoDBTableName" : {
      "Description" : "Name of the DynamoDB table",
      "Type" : "String",
      "Default" : "lambda-apigateway-cf-<yourName>",
      "AllowedPattern" : "[a-zA-Z0-9_.-]+"
    }
  },
  "Resources" : {
    "LambdaAssumedRole": {
      "Type": "AWS::IAM::Role",
      "Properties": {
        "AssumeRolePolicyDocument": {
          "Version": "2012-10-17",
          "Statement": [
            {
              "Effect": "Allow",
              "Principal": {
                "Service": [
                  "lambda.amazonaws.com"
                ]
              },
              "Action": [
                "sts:AssumeRole"
              ]
            }
          ]
        },
        "Policies": [
          {
            "PolicyName": "lambda-apigateway-policy-mc",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Action": [
                    "dynamodb:DeleteItem",
                    "dynamodb:GetItem",
                    "dynamodb:PutItem",
                    "dynamodb:Query",
                    "dynamodb:Scan",
                    "dynamodb:UpdateItem"
                  ],
                  "Effect": "Allow",
                  "Resource": "*"
                },
                {
                  "Resource": "*",
                  "Action": [
                    "logs:CreateLogGroup",
                    "logs:CreateLogStream",
                    "logs:PutLogEvents"
                  ],
                  "Effect": "Allow"
                }
              ]
            }
          }
        ]
      }
    },
    "LambdaFunction": {
      "Type": "AWS::Lambda::Function",
      "DependsOn" : "LambdaAssumedRole",
      "Properties": {
        "Handler": "index.handler",
        "Role": {
          "Fn::GetAtt" : [
            "LambdaAssumedRole",
            "Arn"
          ]
        },
        "Runtime": "python3.7",
        "Code": {
          "ZipFile": {
            "Fn::Join": [
              "\n",
              [
                "from __future__ import print_function",

                "import boto3",
                "import json",

                "print('Loading function')",
                "def handler(event, context):",
                "     '''Provide an event that contains the following keys:",

                "     - operation: one of the operations in the operations dict below",
                "     - tableName: required for operations that interact with DynamoDB",
                "     - payload: a parameter to pass to the operation being performed",
                "     '''",
                "     #print(\"Received event: \" + json.dumps(event, indent=2))",

                "     operation = event['operation']",

                "     if 'tableName' in event:",
                "       dynamo = boto3.resource('dynamodb').Table(event['tableName'])",

                "     operations = {",
                "       'create': lambda x: dynamo.put_item(**x),",
                "       'read': lambda x: dynamo.get_item(**x),",
                "       'update': lambda x: dynamo.update_item(**x),",
                "       'delete': lambda x: dynamo.delete_item(**x),",
                "       'list': lambda x: dynamo.scan(**x),",
                "       'echo': lambda x: x,",
                "       'ping': lambda x: 'pong'",
                "     }",

                "     if operation in operations:",
                "       return operations[operation](event.get('payload'))",
                "     else:",
                "       raise ValueError('Unrecognized operation \"{}\"'.format(operation))"
              ]
            ]
          }

        }
      }
    },
    "ApiGatewayRestApi" : {
      "Type" : "AWS::ApiGateway::RestApi",
      "Properties" : {
        "Name": "DynamoDBOperationsCF",
        "Parameters": {
          "endpointConfigurationTypes": "EDGE"
        }
      }
    },
    "ApiResource" : {
      "Type" : "AWS::ApiGateway::Resource",
      "Properties" : {
        "RestApiId" : {
          "Ref" : "ApiGatewayRestApi"
        },
        "ParentId" : {
          "Fn::GetAtt" : [
            "ApiGatewayRestApi",
            "RootResourceId"
          ]
        },
        "PathPart": "DynamoDBManager"
      }
    },
    "ApiMethod" : {
      "Type" : "AWS::ApiGateway::Method",
      "Properties" : {
        "HttpMethod" : "POST",
        "ResourceId" : {
          "Ref" : "ApiResource"
        },
        "RestApiId" : {
          "Ref" : "ApiGatewayRestApi"
        },
        "AuthorizationType" : "NONE",
        "Integration" : {
          "Type" : "AWS",
          "IntegrationHttpMethod" : "POST",
          "Uri" : {
            "Fn::Sub" : "arn:aws:apigateway:${AWS::Region}:lambda:path/2015-03-31/functions/${LambdaFunction.Arn}/invocations"
          },
          "IntegrationResponses" : [
            {
              "StatusCode" : 200,
              "ResponseTemplates": {
                "application/json" : ""
              }
            }
          ]
        },
        "MethodResponses": [
          {
            "StatusCode" : 200,
            "ResponseModels" : {
              "application/json" : "Empty"
            }
          }
        ]
      }
    },
    "ApiGatewayDeployment" : {
      "Type" : "AWS::ApiGateway::Deployment",
      "DependsOn" : "ApiMethod",
      "Properties" : {
        "RestApiId" : {
          "Ref": "ApiGatewayRestApi"
        },
        "StageName" : "prod"
      }
    },
    "LambdaApiGatewayInvoke": {
      "Type" : "AWS::Lambda::Permission",
      "DependsOn" : [
        "ApiGatewayRestApi",
        "LambdaFunction"
      ],
      "Properties" : {
        "Action" : "lambda:InvokeFunction",
        "Principal" : "apigateway.amazonaws.com",
        "FunctionName": {
            "Fn::GetAtt": [
                "LambdaFunction",
                "Arn"
            ]
        },
        "SourceArn": {
          "Fn::Join": [
            "",
            [
              "arn:",
              {
                "Ref": "AWS::Partition"
              },
              ":execute-api:",
              {
                "Ref": "AWS::Region"
              },
              ":",
              {
                "Ref": "AWS::AccountId"
              },
              ":",
              {
                "Ref": "ApiGatewayRestApi"
              },
              "/prod/POST/DynamoDBManager"
            ]
          ]
        }
      }
    },
    "DynamoDBTable" : {
      "Type" : "AWS::DynamoDB::Table",
      "Properties" : {
        "AttributeDefinitions" : [
          {
            "AttributeName" : "id",
            "AttributeType" : "S"
          }
        ],
        "KeySchema" : [
          {
            "AttributeName" : "id",
            "KeyType" : "HASH"
          }
        ],
        "ProvisionedThroughput" : {
          "ReadCapacityUnits" : "5",
          "WriteCapacityUnits" : "5"
        },
        "TableName" : {
          "Ref" : "DynamoDBTableName"
        }
      }
    }
  },
  "Outputs" : {
    "ApiGatewayInvokeURL" : {
      "Description": "Invoke URL via url",
      "Value" : {
        "Fn::Sub" : "https://${ApiGatewayRestApi}.execute-api.${AWS::Region}.amazonaws.com/prod/DynamoDBManager"
      }
    },
    "DynamoDBName" : {
      "Description" : "Name of the created DynamoDB table",
      "Value" : {
        "Ref" : "DynamoDBTableName"
      }
    }
  }
}
```
