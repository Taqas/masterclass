# Deploy API

The API as mentioned in the [tutorial](https://docs.aws.amazon.com/lambda/latest/dg/services-apigateway-stutorial.html)
consists of the following AWS services

- IAM
- Lambda
- API Gateway
- DynamoDB

The full code can be found [here](Answers/api.md). To deploy the API, we need to give the DynamoDB table a unique name, so in the `Parameters` section, change the default name of the table. Note the resource that creates the DynamoDB table, and see that it is equal to the one you've deployed earlier, except for the name which references a parameter.

```json
"Default" : "lambda-apigateway-cf-<yourName>"
```
Now deploy the stack and try to send a record, for example.

```bash
URL=<url>
TABLENAME=<tableName>
curl -X POST -d "{\"operation\":\"create\",\"tableName\":\"$TABLENAME\",\"payload\":\
{\"Item\":{\"id\":\"2\",\"name\":\"Wong\"}}}" $URL
```

Note that the prettified json that is send in the command above is
```json
{
    "operation": "create",
    "tableName": "<tableName>",
    "payload": {
        "Item": {
            "id": "1",
            "name": "Bob"
        }
    }
}
```
