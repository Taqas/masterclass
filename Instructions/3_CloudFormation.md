# CloudFormation

Now, we're going to create a DynamoDB table using [CloudFormation](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/Welcome.html).

## Create a CloudFormation template
A template is used so that CloudFormation knows what resources to deploy, and how it should be configured. A valid template only needs the section 'Resources'.

Let's create a file `cfTemplate.json` in `Workshop`

```bash
touch Workshop/cfTemplate.json
```

Choose you're prefered IDE to edit the file, and add a section Resources.

```json
{
  "Resources" : {
    <resources go here>
  }
}
```

We are going to create a DynamoDB table, see documentation and examples [here](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-dynamodb-table.html).

Add a section with your prefered name, for example `DynamoDBTable` of type `AWS::DynamoDB::Table` to your template.

```JSON
{
  "Resources" : {
    "DynamoDBTable" : {
      "Type" : "AWS::DynamoDB::Table"
    }
  }
}
```

## Deployment

Let's try to deploy a DynamoDB table without any configuration, and see what happens.

Since we will be using the CloudFormation service, we will use
```bash
aws cloudformation
```
If you want more information use
```bash
aws cloudformation help
```
You will get a description and an overview of all the available functions.
We will be using `deploy` to deploy our stack. More information about this function can be obtained by using
```bash
aws cloudformation deploy help
```
It says two parameters are required, `template-file` and `stack-name`. Try to deploy a stack yourself. If you're stuck see [answer](Answers/deploymentstatement.md)

Next, head to the console, search for `CloudFormation` and look at the `Events` of your stack.
You will see the following message
`DynamoDBTable	CREATE_FAILED	You must specify a KeySchema list.`
The creation failed, because a DynamoDB table needs to be deployed with a set of configuration parameters. Let's add a KeySchema list as a property to `Workshop/cfTemplate.json`, with AttributeName equal to `id`. Try yourself or see the [answer](Answers/keyschema.md).

Try deploying your stack again.

You will get the following error message  
`An error occurred (ValidationError) when calling the CreateChangeSet operation: Stack:arn:aws:cloudformation:eu-west-1:<>:stack/<yourStackName>/<CfStackID is in ROLLBACK_COMPLETE state and can not be updated.`  
The reason you get this message is that the stack couldn't be deployed the first time, we have to delete the stack completely and start over. For more information about the statusCodes of the stack, see [here](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-cfn-describing-stacks.html).

Try deleting using the `delete-stack` function. Try yourself, or see the [answer](Answers/deletestack.md).

If you then deploy your stack again, you will notice that it now says   
`You must specify the AttributeDefinitions property.`

You can look through the documentation to find all the necessary configuration details, or you can find the answer [here](Answers/minimumproperties.md).

Try to convert it yourself to valid JSON, or see the answer [here](validcreatetable.md).

Well done! You have now created a DynamoDB table with just code.

## Changed configuration

Now try to change `ReadCapacityUnits` to `10`, and deploy your stack again. What happens?
