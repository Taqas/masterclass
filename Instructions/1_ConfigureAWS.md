# Configure AWS account

To begin, we need an AWS account. The following credentials are given by the instructor
- Username  
- Password  
- AccessKey  
- SecretAccesKey

In this tutorial, we will use two different ways to interact with AWS - via the console and the Command Line Interface (CLI).

## Console

To log into the [AWS Console](https://aws.amazon.com), you need the Username, Password, and Account ID (or it's alias). The alias of our Account ID is the company name without spaces, in lowercase.

## Command Line Interface (CLI)
The AccessKey and SecretAccesKey are used to interact with AWS via the command line.

First we need to install the AWS CLI, see the official instructions [here](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-mac.html).

If you succeeded, run the following
```bash
$ which aws
/usr/local/bin/aws
$ aws --version
aws-cli/2.0.0 Python/3.7.4 Darwin/18.7.0 botocore/2.0.0
```

Now, configure AWS with the provided credentials
```bash
$ aws configure
AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE
AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
Default region name [None]: eu-west-1
Default output format [None]: ENTER
```

To check if you have access to the Future Facts account, try running the following

```bash
aws dynamodb list-tables
```
