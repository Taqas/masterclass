# AWS console

To get a feeling with the console we will create a DynamoDB table via the console,
first and later create a table via the CLI.

Search for the service DynamoDB.
Make sure in the top right corner the region 'Europe (Ireland)' is selected.
Check whether the same tables are listed as when you ran `aws dynamodb list-tables`

Now click on create table and enter the following
```
Table name* = 'console<yourName>'
Primary key* = 'id' - 'String'
```
Note the following `default settings`, it will become relevant later
```
Provisioned capacity set to 5 reads and 5 writes.
```
If you click `Create`, you'll get an error that you don't have access to create a
SNS topic, but that irrelvant for now.
Click on `Continue to table`.

We have created a DynamoDB table via the console!
