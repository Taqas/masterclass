# Workshop IaC using Cloudformation

Today, what we're trying to achieve is creating a public API that is able to write
and read from a DynamoDB database with CloudFormation.

This workshop is based off a [tutorial](https://docs.aws.amazon.com/lambda/latest/dg/services-apigateway-stutorial.html) from AWS.

If you follow the `Instructions`, you will create a file in the directory `Workshop`. If you're stuck, you can find answers in the directory `Answers`.
